import Constants from './constants';

// ------------------------------------
// Action Handlers
// ------------------------------------
const
  ACTION_HANDLERS = {
    [Constants.UPDATE_USER_DETAILS]: (state, action) => {
      return {
        ...state,
        userDetails: action.payload,
      };
    },
  },

  initialState = {
    userDetails: {}
  };

export default function (state = initialState, action) {
  const handler = ACTION_HANDLERS[action.type];

  return handler ? handler(state, action) : state;
}

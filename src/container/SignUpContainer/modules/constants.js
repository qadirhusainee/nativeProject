import mirrorkey from 'mirrorkey';

export default mirrorkey([
  'REQUEST_BEGIN',
  'SIGNIN_END',
  'SIGNIN_ERROR_END',
  'GET_QUESTION_STATUS',
  'GET_USER_DETAILS',
  'SIGNOUT_END',
]);

// @flow
import React, { Component } from "react";
import { connect } from "react-redux";
import SignUp from "../../stories/screens/SignUp";

export default class SignUpContainer extends Component {

	render() {
		return <SignUp navigation={this.props.navigation} data={'Sign Up Page'} />;
	}
}


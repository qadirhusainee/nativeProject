import Constants from './constants';

// ------------------------------------
// Action Handlers
// ------------------------------------
const
  ACTION_HANDLERS = {
    [Constants.FETCH_AILMENT_LIST]: (state, action) => {
      return {
        ...state,
        userDetails: action.payload,
      };
    },
  },

  initialState = {
    ailmentList: {},
    fetching: true,
  };

export default function (state = initialState, action) {
  const handler = ACTION_HANDLERS[action.type];

  return handler ? handler(state, action) : state;
}

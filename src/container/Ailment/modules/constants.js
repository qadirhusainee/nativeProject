import mirrorkey from 'mirrorkey';

export default mirrorkey([
  'FETCH_AILMENT_LIST',
  'FETCH_AILMENT_LIST_END',
]);

// @flow
import React from "react";
import { StackNavigator, DrawerNavigator } from "react-navigation";
import { Root } from "native-base";
import Home from "./container/HomeContainer";
import SignUp from "./container/SignUpContainer";
import MarkettingPages from "./container/MarkettingPages";

// import BlankPage from "./container/BlankPageContainer";
// import Sidebar from "./container/SidebarContainer";

// const Drawer = DrawerNavigator(
// 	{
// 		Home: { screen: Home },
// 	},
// 	{
// 		initialRouteName: "Home",
// 		contentComponent: props => <Sidebar {...props} />,
// 	}
// );

const App = StackNavigator(
	{
		Home: { screen: Home },
		SignUp: { screen: SignUp },
		MarkettingPages: {screen: MarkettingPages}
		// Ailment: { screen: Drawer },
		// Exercise: { screen: Drawer }
	},
	{
		initialRouteName: "Home",
		headerMode: "none",
	}
);

export default () => (
	<Root>
		<App />
	</Root>
);

import React from "react";
import {
  Container,
  Content,
  Text,
} from "native-base";
import HeaderBar from '../../Common/Header'

import styles from "./styles";

const SignUp = ({
  data
}) => {
  return (
    <Container style={styles.container}>
      <HeaderBar />
      <Content>
        <Text>{data}</Text>
      </Content>
    </Container>
  );
}

export default SignUp;

import React from 'react';
import { Text, View , Button, Alert , Image  } from 'react-native';
import Swiper from 'react-native-swiper';
//import styles from './style';
import { responsiveHeight, responsiveWidth, responsiveFontSize } from 'react-native-responsive-dimensions';
import mp1 from '../../../../assets/images/markettingPages/mp1.png';
import mp2 from '../../../../assets/images/markettingPages/mp2.png'


export default class marketingScreen extends React.Component {

constructor(){
  super();
  this.jumpSlide  = this.jumpSlide.bind(this);
  this.onDone = this.onDone.bind(this);
}

jumpSlide = () => {
  this.refs.mySwiper.scrollBy(1); // where n is the number of slides to move
};

onDone = () => {
  Alert.alert("Done with marketing Slides. Thank-you");
}

  render(){
    return(
      <View style={{flex: 1}}>
        <Swiper paginationStyle={{ bottom: 70 }}  loop={false} ref='mySwiper'>
      	  <View style={styles.slide1}>
						<Image source={mp1} style={styles.image} />
						<View style={styles.button} >
							<Button onPress={this.jumpSlide} title="Skip"  color="#8ec25a"/>
        		</View>
        	</View>
					<View style={styles.slide1}>
						<Image source={mp2} style={styles.image}/>
						<View style={styles.button} >
							<Button   onPress={this.jumpSlide} title="Skip"  color="#8ec25a"/>
						</View>
					</View>
					<View style={styles.slide3}>
						<Image source={mp1} style={styles.image}/>
						<View style={styles.button} >
							<Button  onPress={this.onDone} title="Done"  color="#4a88db"/>
						</View>
					</View>
        </Swiper>
        </View>
    )
  }
}
const styles = {
  wrapper: {
  },
  slide1: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#ffffff',
    height : 600
  },
  slide2: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#ffffff',
    height : 600
  },
  slide3: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#ffffff',
    height : 600
  },
  text: {
    color: '#fff',
    fontSize: 30,
    fontWeight: 'bold'
  },
  image:{
    height : responsiveHeight(90),
    width :responsiveWidth(100),
    marginRight : 5
  },
  button:{
    // marginTop: 10,
    borderRadius:20,
    width:300,
    marginBottom : 5,
    justifyContent: 'center',
  },
  swiperDot: {
  marginTop:100,
  },
}

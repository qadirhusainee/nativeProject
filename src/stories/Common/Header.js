import React from "react";
import {
  Header,
  Title,
  Content,
  Text,
  Button,
  Icon,
  Left,
  Body,
  Right,
  List,
  ListItem
} from "native-base";
import { StyleSheet } from "react-native";

const HeaderBar = ({
  navigation
}) => {
  return (
    <Header>
      {/* <Left>
        <Button transparent>
          <Icon
            active
            name="menu"
            onPress={() => navigation.navigate("SignUp")}
          />
        </Button>
      </Left> */}
      <Body>
        <Title>HPS</Title>
      </Body>
      <Right />
    </Header>
  );
}


export default HeaderBar;


import React, { PureComponent } from 'react';
import { View, Text, NetInfo, Dimensions, StyleSheet } from 'react-native';

const { width } = Dimensions.get('window');

function MiniOfflineSign({data}){
  return (
    <View style={styles.offlineContainer}>
      <Text style={styles.offlineText}>No Internet Connections {data}</Text>
    </View>
  );
}

function MiniOnlineSign() {
  return (
    <View style={styles.offlineContainer}>
      <Text style={styles.offlineText}>Online</Text>
    </View>
  );
}

class OfflineNotice extends PureComponent {
  state = {
    isConnected: true
  };

  componentDidMount() {
    NetInfo.getConnectionInfo().then((connectionInfo) => {
    this.setState({
        connectionInfo: connectionInfo.type
    })
    console.log('Initial, type: ' + connectionInfo.type + ', effectiveType: ' + connectionInfo.effectiveType);
  });

  NetInfo.addEventListener(
    'connectionChange',
    this.handleConnectivityChange
  );
  }

  componentWillUnmount() {
    NetInfo.removeEventListener('connectionChange', this.handleConnectivityChange);
  }

  handleConnectivityChange = connectionInfo => {
    console.log('Connections',connectionInfo)
    if (isConnected) {
      this.setState({ isConnected: connectionInfo.type });
    } else {
      this.setState({ isConnected: connectionInfo.type });
    }
  };

  render() {
    console.log('state',this.state.isConnected)
    if (!this.state.isConnected) {
      return <MiniOfflineSign data={this.state.isConnected} />;
    }
    return null
  }
}

const styles = StyleSheet.create({
  offlineContainer: {
    backgroundColor: '#b52424',
    height: 30,
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
    width,
    position: 'absolute',
    top: 50,
    zIndex:1
  },
  offlineText: { color: '#fff' }
});

export default OfflineNotice;